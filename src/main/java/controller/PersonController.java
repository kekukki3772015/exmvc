package controller;

import java.lang.ProcessBuilder.Redirect;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import view.PersonForm;
import dao.PersonDao;

@Controller
public class PersonController {
	
	@Resource
	private PersonDao dao;
	
	@RequestMapping("/")
	public String home(){
		return "redirect:/list";
	}
	
	@RequestMapping("/hello")
	public String hello(ModelMap model){
		
		model.addAttribute("message", "Hello world!");
		
		return "hello";
	}

	@RequestMapping("/list")
	public String list(ModelMap model){
		
		model.addAttribute("persons", dao.findAll());
		
		return "list";
	}

	@RequestMapping("/delete/{personId}")
	public String delete(@PathVariable("personId") Long personId){
		
		dao.delete(personId);
			
		return "redirect:/list";
	}
	
	@RequestMapping("/edit/{personId}")
	public String edit(
			@ModelAttribute("personForm") PersonForm form,
			@PathVariable("personId") Long personId){
		
		form.setPerson(dao.findById(personId));
		form.setAgeGroups(dao.getAgeGroups());	
		
		return "form";
	}

	@RequestMapping("/add")
	public String add(@ModelAttribute("personForm") PersonForm form){
		
		form.setAgeGroups(dao.getAgeGroups());
		
		return "form";
	}
	
	@RequestMapping("/save")
	public String save(@ModelAttribute("personForm") PersonForm form){
		
		dao.store(form.getPerson());
		
		return "redirect:/list";
	}
}