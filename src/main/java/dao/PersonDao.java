package dao;

import java.util.*;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import domain.Person;

@Repository
public class PersonDao {

    private Long counter = 1L;

    private List<Person> personStore = new ArrayList<Person>();

    @PostConstruct
    public void init() {
        personStore.add(new Person(counter++, "Tiit", "Kask", 1));
        personStore.add(new Person(counter++, "Mari", "Lepp", 2));
        personStore.add(new Person(counter++, "Teet", "Kuusk", 3));
    }

    public Map<Integer, String> getAgeGroups() {
        LinkedHashMap<Integer, String> map = new LinkedHashMap<Integer, String>();
        map.put(1, "Laps");
        map.put(2, "Täiskasvanu");
        map.put(3, "Pensionär");
        return map;
    }


    public void store(Person person) {
        if (person.getId() != null) {
            delete(person.getId());
        } else {
            person.setId(counter++);
        }
        personStore.add(person);
    }

    public List<Person> findAll() {
        return new ArrayList<Person>(personStore);
    }

    public void delete(Long personId) {
        List<Person> remaining = new ArrayList<Person>();
        for (Person person : personStore) {
            if (!person.getId().equals(personId)) {
                remaining.add(person);
            }
        }
        personStore = remaining;
    }

    public Person findById(Long personId) {
        for (Person person : personStore) {
            if (person.getId().equals(personId)) {
                return person;
            }
        }

        throw new IllegalStateException("can't find: " + personId);
    }

}
