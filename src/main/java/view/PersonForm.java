package view;

import java.util.Map;

import domain.Person;

public class PersonForm {

    private Person person;

    private Map<Integer, String> ageGroups;

    private String message;
    
    private Boolean disabled;

    @Override
	public String toString() {
		return "PersonForm [person=" + person + ", ageGroups=" + ageGroups
				+ "]";
	}

	public Map<Integer, String> getAgeGroups() {
        return ageGroups;
    }

    public void setAgeGroups(Map<Integer, String> ageGroups) {
        this.ageGroups = ageGroups;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

}
