package domain;

public class Person {

    private Long id;
    private String firstName;
    private String name;
    private Integer ageGroupId;

    public Person() {}

    public Person(Long id, String firstName, String name, int ageGroupId) {
        this.id = id;
        this.firstName = firstName;
        this.name = name;
        this.ageGroupId = ageGroupId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Integer getAgeGroupId() {
        return ageGroupId;
    }
    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", firstName=" + firstName + ", ageGroupId=" + ageGroupId + "]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
